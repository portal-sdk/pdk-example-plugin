Portal 2 Development Kit example plugin
-
To start, clone this repo recursively to get all submodules
```bash
$ git clone https://gitlab.com/portal-sdk/pdk-example-plugin --recursive
```

Then, open the cmake project with your favorite IDE/Code editor
To compile, do
```
$ mkdir build
$ cd build
$ cmake ..
$ ninja
```
